import express from 'express'
import passport from 'passport'


const router = express.Router()

router.get('/', (req, res) => {
  res.send('AUTH Route Works')
})

router.post('/login',
  passport.authenticate('local'), (req, res) => {
    res.redirect('/')
  }
)

router.get('/logout', (req, res, next) => {
    req.logout()
    res.send()
  }
)

router.get('/user', (req, res) => {
  res.json(req.user )
})

export default router
