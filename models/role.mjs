import db from '.'

const table = 'roles'

export function all () {
  return db.getAll(table)
}

export function add (params) {
  return db.insert(table, params)
}

export function get (id) {
  return db.get(table, id)
}

export function upd (id, params) {
  return db.update(table, id, params)
}

export function del (id) {
  return db.delete(table, id)
}

export default { all, add, get, upd, del }
