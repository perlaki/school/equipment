import pgp from 'pg-promise'

const methods = {
  extend(obj, dc) {
    obj.getAll = async function (table) {
      try {
        return await obj.any('SELECT * FROM $1~', table)
      } catch (e) {
        console.error(e)
      }
    }
    obj.get = async function (table, id) {
      try {
        return await obj.one('SELECT * FROM $1~ WHERE id = $2', [table, id])
      } catch (e) {
        console.error(e)
      }
    }
    obj.insert = async function (table, params) {
      try {
        return await obj.one('INSERT INTO $1~ ($2^) VALUES ($3^) RETURNING *', [table, Object.keys(params).join(','), `'${Object.values(params).join("','")}'`])
      } catch (e) {
        console.error(e)
      }
    }
    obj.update = async function (table, id, params) {
      try {
        delete params.id
        for (let prop of Object.keys(params)) if (params[prop] == null) delete params[prop]
        return await obj.one('UPDATE $1~ SET $3^ WHERE id = $2 RETURNING *', [table, id, Object.keys(params).map(x => `${x} = '${params[x]}'`).join(",")])
      } catch (e) {
        console.error(e)
      }
    }
    obj.delete = async function (table, id) {
      try {
        return await obj.one('DELETE FROM $1~ WHERE id = $2', [table, id])
      } catch (e) {
        console.error(e)
      }
    }
  }
}

const connection = {
  host: 'localhost',
  port: 5432,
  database: 'equipment',
  user: 'liam',
  password: 'm4!l0viE'
}

const db = pgp(methods)(connection)

export default db;

export { default as Item } from "./item";
export { default as User} from "./user";
export { default as Role} from "./role";
