import Vue from 'vue'
import './plugins/vuetify'
import './plugins/axios'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.directive('restricted', {async bind(el, binding) {
  try {
    let res = await Vue.axios.get('/auth/user')
    if (!binding.value.includes(res.data.role)) el.remove()
  } catch (error) {
    console.log(error)
  }
}})


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
